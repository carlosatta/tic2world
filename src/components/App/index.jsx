import React, { Component } from 'react'
import * as THREE from 'three';


import Stats from 'three/examples/jsm/libs/stats.module';

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader';
// import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
// import { GUI } from 'three/examples/jsm/libs/dat.gui.module'

import './style.css';


class App extends Component {
  constructor(props) {
    super(props)

    this.start = this.start.bind(this)
    this.stop = this.stop.bind(this)
    this.animate = this.animate.bind(this)
    this.onWindowResize = this.onWindowResize.bind(this)
  }

  componentDidMount() {
    const self = this;

    // const gui = new GUI();

    const camera = new THREE.PerspectiveCamera(
      45,
      window.innerWidth / window.innerHeight,
      1,
      2000
    );
    camera.position.set( 0, 850, 0 );

    // const cameraFolder = gui.addFolder('Camera')
    // cameraFolder.add(camera.position, 'x', 0, 1000)
    // cameraFolder.add(camera.position, 'y', 0, 1000)
    // cameraFolder.add(camera.position, 'z', 0, 1000)
    // cameraFolder.open()

    const scene = new THREE.Scene();

    const hemiLight = new THREE.HemisphereLight( 0xffffff, 0x444444);
    hemiLight.position.set( 0, 50, 0 );
    scene.add( hemiLight );


    const dirLight = new THREE.DirectionalLight(0xffffff);
    dirLight.position.set( 0, 200, 100 );
    dirLight.castShadow = true;
    dirLight.shadow.camera.top = 180;
    dirLight.shadow.camera.bottom = - 100;
    dirLight.shadow.camera.left = - 120;
    dirLight.shadow.camera.right = 120;
    scene.add( dirLight );

    const loader = new FBXLoader();
    // const loader = new GLTFLoader();
    // loader.load('./models/cute-little-planet.glb', (gltf) => {

    loader.load('./models/Sketch_18.FBX', (object) => {

      console.log(object);


      object.traverse( function ( child ) {

        if ( child.isMesh ) {

          child.castShadow = true;
          child.receiveShadow = true;

        }

      } );

      scene.add( object );

      // const model = gltf.scene
      // self.world = model;

      // self.world.rotation.x = 5;
      // self.world.rotation.y = 5;

      // self.scene.add(model);
    }, (d) => {
    }, (err) => {
      console.log(err);
    })


    const grid = new THREE.GridHelper( 2000, 20, 0x000000, 0x000000 );
    grid.material.opacity = 0.2;
    grid.material.transparent = true;

    const renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true })
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.setClearColor( 0x000000, 0 );
    renderer.shadowMap.enabled = true;

    const controls = new OrbitControls( camera, renderer.domElement );
    controls.maxDistance = 1000;
    controls.minDistance = 300;
    controls.target.set( 0, 0, 0 );
    controls.update();


    this.scene = scene;
    this.camera = camera;
    this.dirLight = dirLight;
    this.controls = controls;
    this.renderer = renderer;

    this.mount.appendChild(this.renderer.domElement)
    this.start()

    this.stats = new Stats();
		this.mount.appendChild( this.stats.dom );

    window.addEventListener( 'resize', this.onWindowResize, false );
  }

  onWindowResize() {

    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize( window.innerWidth, window.innerHeight );

}

  componentWillUnmount() {
    this.stop()
    this.mount.removeChild(this.renderer.domElement)
  }

  start() {
    if (!this.frameId) {
      this.frameId = requestAnimationFrame(this.animate)
    }
  }

  stop() {
    cancelAnimationFrame(this.frameId)
  }

  animate() {

    if (!!this.world) {
      // this.world.rotation.x += 0.002;
      // this.world.rotation.y += 0.001;
    }

    if (!!this.dirLight && !!this.camera) {
      // console.log(this.camera.getWorldPosition());
      // this.dirLight.position.copy( this.camera.getWorldPosition() );
    }

    this.renderScene()
    this.stats.update();
    this.frameId = window.requestAnimationFrame(this.animate)
  }

  renderScene() {
    this.renderer.render(this.scene, this.camera)
  }

  render() {
    return (
      <div
        ref={(mount) => { this.mount = mount }}
      />
    )
  }
}

export default App
